﻿using FullStackAPI.Data;
using FullStackAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FullStackAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EmployeeController : Controller
    {
        private readonly FullStackDbContext _FullStackDbContext;

        public EmployeeController(FullStackDbContext fullStackDbContext)
        {
            this._FullStackDbContext = fullStackDbContext;
        }

        [HttpGet]
        public async Task<IActionResult> GetEmployees()
        {
            return Ok(await _FullStackDbContext.Employees.ToListAsync());
        }

        [HttpGet]
        [Route("{id:Guid}")]
        public async Task<IActionResult> GetEmployee ([FromRoute] Guid id)
        {
            var employee = await _FullStackDbContext.Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }
            return Ok(employee);

        }
        

        [HttpPost]
        public async Task<IActionResult> PostEmployees([FromBody] Employee employeeRequest)
        {
            employeeRequest.Id = Guid.NewGuid();

            await _FullStackDbContext.Employees.AddAsync(employeeRequest);
            await _FullStackDbContext.SaveChangesAsync();
            return Ok(employeeRequest);

        }


        [HttpPut]
        [Route("{id:Guid}")]
        public async Task<IActionResult> PutEmployee([FromRoute] Guid id,Employee updateEmployeeRequest)
        {
           var employee= await _FullStackDbContext.Employees.FindAsync(id);

            if (employee == null)
            {
                return NotFound();
            }

            employee.Name = updateEmployeeRequest.Name;
            employee.Email = updateEmployeeRequest.Email;
            employee.Phone = updateEmployeeRequest.Phone;
            employee.Salary=updateEmployeeRequest.Salary;
            employee.Department = updateEmployeeRequest.Department;
            await _FullStackDbContext.SaveChangesAsync();
            return Ok(employee);
        }

        [HttpDelete]
        [Route("{id:guid}")]
        public async Task<IActionResult> DeleteEmployee([FromRoute] Guid id)
        {
            var employee = await _FullStackDbContext.Employees.FindAsync(id);
            if (employee != null)
            {
                _FullStackDbContext.Remove(employee);
                await _FullStackDbContext.SaveChangesAsync();
                return Ok();
            }
            return NotFound();
        }
    }
}
